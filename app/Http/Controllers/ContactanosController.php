<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreContactanos;
use App\Mail\ContactanosMailable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactanosController extends Controller
{

    public function index()
    {

        return view('contactanos.index');
    }

    public function store(StoreContactanos $request)
    {
        $correo = new ContactanosMailable($request->all());

        Mail::to('lgo80@yahoo.com.ar')->send($correo);

        return redirect()->route('contactanos.index')
            ->with(
                'info',
                'La consulta fue enviada correctamente'
            );
    }
}
