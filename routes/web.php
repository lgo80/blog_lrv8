<?php

use App\Http\Controllers\ContactanosController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\pruebaController;
use App\Http\Controllers\CursoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
El mensaje de entrada
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', homeController::class)->name('home');
Route::resource('cursos', CursoController::class);
Route::view('nosotros', 'nosotros')->name('nosotros');
Route::resource('contactanos', ContactanosController::class);

/*
Esto es cuando despues de haber trabajo con un nombre de ruta y quisiste cambiarlo
para no cambiar todas las rutas del sistema se cambia con names y parameters.
Route::resource('asignaturas', CursoController::class)
    ->parameters(['asignaturas' => 'curso'])
    ->names('cursos');
    */

/*Route::get('cursos', [CursoController::class,'index'])->name('cursos.index');
Route::get('cursos/create', [CursoController::class,'create'])->name('cursos.create');
Route::get('cursos/{curso}', [CursoController::class,'show'])->name('cursos.show');
Route::get('cursos/{curso}/edit', [CursoController::class,'edit'])->name('cursos.edit');
Route::post('cursos', [CursoController::class,'store'])->name('cursos.store');
Route::put('cursos/{curso}', [CursoController::class,'update'])->name('cursos.update');
Route::delete('cursos/{curso}', [CursoController::class,'destroy'])->name('cursos.destroy');*/
/*Route::get('cursos/{curso}', function ($curso) {
    return 'Curso: ' . $curso;
});*/

/*
    Si quiero que un parametro sea opcional hay que agregar un signo ? 
    (al final de la variable)

Route::get('cursos/{curso}/{categoria?}', function ($curso, $categoria = null) {
    return 'Curso: ' . $curso . ' categoria: ' . $categoria;
});*/