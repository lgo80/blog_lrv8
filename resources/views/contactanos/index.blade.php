@extends('layouts.plantilla')

@section('title', 'Contactanos')

@section('content')
    <h1>Deje un mensaje</h1>
    <form action="{{ route('contactanos.store') }}" method="POST">
        @csrf
        <label>
            Nombre:
            <br>
            <input type="text" name="nombre" />
        </label>
        @error('nombre')
            <p><strong>{{ $message }}</strong></p>
        @enderror
        <br>
        <label>
            Correo:
            <br>
            <input type="text" name="correo" />
        </label>
        @error('correo')
            <p><strong>{{ $message }}</strong></p>
        @enderror
        <br>
        <label>
            Asunto:
            <br>
            <input type="text" name="asunto" />
        </label>
        <br>
        <label>
            mensaje:
            <br>
            <textarea name="mensaje" rows="5"></textarea>
        </label>
        @error('mensaje')
            <p><strong>{{ $message }}</strong></p>
        @enderror
        <br>
        <button type="submit">Enviar consulta</button>
    </form>

    @if (session('info'))
        <script>
            alert("{{session('info')}}");

        </script>
    @endif
@endsection
