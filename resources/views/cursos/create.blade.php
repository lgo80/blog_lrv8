@extends('layouts.plantilla')

@section('title', 'Crear curso')

@section('content')
    <h1>Estas en la pagina de crear cursos</h1>
    <form action="{{ route('cursos.store') }}" method="POST">
        @csrf
        <label for="name">Nombre: </label><br />
        <input type="text" name="name" id="name" value="{{ old('name') }}" /><br />
        @error('name')
            <small>* {{ $message }}</small>
        @enderror
        <br /><br />
        <label for="description">Descripción: </label><br />
        <textarea rows="5" name="description" id="description">{{ old('description') }}</textarea><br />
        @error('description')
            <small>* {{ $message }}</small>
        @enderror
        <br /><br />
        <label for="category">Categoria: </label><br />
        <input type="text" name="category" id="category" value="{{ old('category') }}" /><br />
        @error('category')
            <small>* {{ $message }}</small>
        @enderror
        <br /><br />
        <button type="submit">Grabar</button>
    </form>
@endsection
