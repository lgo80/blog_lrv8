@extends('layouts.plantilla')

@section('title', 'Editar curso')

@section('content')
    <h1>Estas en la pagina de crear cursos</h1>
    <form action="{{ route('cursos.update', $curso) }}" method="POST">
        @csrf
        @method('put')
        <label for="name">Nombre: </label><br />
        <input type="text" name="name" id="name" value="{{ old('name',$curso->name) }}" /><br />
        @error('name')
            <small>* {{ $message }}</small>
        @enderror
        <br /><br />
        <label for="description">Descripción: </label><br />
        <textarea rows="5" name="description" id="description">{{ old('description',$curso->description) }}</textarea><br />
        @error('description')
            <small>* {{ $message }}</small>
        @enderror
        <br /><br />
        <label for="category">Categoria: </label><br />
        <input type="text" name="category" id="category" value="{{ old('category',$curso->category) }}" /><br />
        @error('category')
            <small>* {{ $message }}</small>
        @enderror
        <br /><br />
        <button type="submit">Editar</button>
    </form>
@endsection
