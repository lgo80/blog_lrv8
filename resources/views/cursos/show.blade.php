@extends('layouts.plantilla')

@section('title', 'Curso elegido')

@section('content')
    <h1>Estas en la pagina de mostrar el curso {{ $curso->name }}</h1>
    <a href="{{ route('cursos.index') }}">Volver a los cursos</a><br>
    <a href="{{ route('cursos.edit', $curso) }}">Editar curso</a>
    <p><strong>Categoria: </strong>{{ $curso->category }}</p>
    <p>{{ $curso->description }}</p>

    <form action="{{ route('cursos.destroy', $curso) }}" method="POST">
        @csrf
        @method('delete')
        <button type="submit">Eliminar</button>
    </form>
@endsection
