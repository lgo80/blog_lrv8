@extends('layouts.plantilla')

@section('title', 'Lista de cursos')

@section('content')
    <h1>Estas en la pagina de cursos</h1>
    <a href="{{ route('cursos.create') }}">Crear curso</a>
    <ul>
        @foreach ($cursos as $curso)
            <li>
                <a href="{{ route('cursos.show', $curso) }}">
                    {{ $curso->name }}
                </a>
            </li>
        @endforeach
    </ul>

    {{ $cursos->links() }}
@endsection
