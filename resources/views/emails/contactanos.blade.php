<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">-->
    <title>Document</title>
</head>

<body>
    <h1>Mandar correo electronico</h1>
    <p>Mi primer mail</p>
    <p><strong>Nombre: </strong>{{$contacto['nombre']}}</p>
    <p><strong>Correo electrónico: </strong>{{$contacto['correo']}}</p>
    <p><strong>Asunto: </strong>{{$contacto['asunto']}}</p>
    <p><strong>Mensaje: </strong>{{$contacto['mensaje']}}</p>
</body>

</html>
